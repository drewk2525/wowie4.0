import BattleScene from "../scenes/BattleScene";
import { collectables } from "../utils/data";
import Collectable from "./Collectable";
import Unit from "./Unit";
const rng = new Phaser.Math.RandomDataGenerator();

export default class Enemy extends Unit {
    displayWidth: number;
    displayHeight: number;
    originalMoveSpeed: number;
    slowSprite: Phaser.GameObjects.Sprite;
    zapSprite: Phaser.GameObjects.Sprite;

    constructor(scene: BattleScene, name: string, unitConfig: unitConfig) {
        super(scene, name, unitConfig);
        // this.mainObj = mainObj;
        // this.container = scene.add.container(mainObj.x, mainObj.y);
        // mainObj.x = mainObj.y = 0;
        this.displayWidth = 70;
        this.displayHeight = 70;
        this.isEnemy = true;
        this.state = "moving";
        this.originalMoveSpeed = this.moveSpeed;
        // @ts-ignore
        this.target = scene.player.container;
        this.body.collideWorldBounds = false;
        this.scene.time.addEvent({
            delay: 200,
            callback: () => {
                if (this.isAlive() && scene.player.isAlive()) {
                    this.move();
                }
            },
        });
    }

    die(isDead) {
        if (!isDead) {
            const item = collectables[rng.between(0, collectables.length - 1)];
            const collectable = new Collectable(this.scene, item, {
                x: this.container.x,
                y: this.container.y,
            });
            this.scene.collectableGroup.add(collectable.container);
        }
        super.die(isDead);
    }

    inBounds() {
        const { x, y } = this.container;
        const border = 20;
        return (
            x > border &&
            x < this.scene.scale.width - border &&
            y > border &&
            y < this.scene.scale.height - border
        );
    }

    slow() {
        if (this.moveSpeed === this.originalMoveSpeed) {
            this.moveSpeed = this.moveSpeed * 0.3;
        }
        if (!this.slowSprite) {
            this.slowSprite = this.scene.add.sprite(0, 0, "slowEnemyIcon");
            this.slowSprite.play({ key: "slowEnemyIcon", repeat: -1 });
            this.container.add(this.slowSprite);
        }
    }

    staticZap() {
        if (!this.zapSprite) {
            this.zapSprite = this.scene.add.sprite(0, 0, "shockEnemyIcon");
            this.zapSprite.play({
                key: "shockEnemyIcon",
                repeat: 0,
                hideOnComplete: true,
            });
            this.container.add(this.zapSprite);
        } else {
            this.zapSprite.visible = true;
            this.zapSprite.play({
                key: "shockEnemyIcon",
                repeat: 0,
                hideOnComplete: true,
            });
        }
    }

    unslow() {
        this.moveSpeed = this.originalMoveSpeed;
        if (this.slowSprite) {
            this.slowSprite.destroy();
            this.slowSprite = null;
        }
    }

    move() {
        const scene = this.scene;
        if (!this.isAlive() || !scene.player.isAlive()) {
            return;
        }
        scene.physics.moveToObject(
            this.container,
            scene.player.container,
            this.moveSpeed
        );
        if (this.container.x < this.scene.player.container.x) {
            this.mainObj.flipX = false;
        } else {
            this.mainObj.flipX = true;
        }
        this.state = "moving";
        this.scene.time.addEvent({
            delay: 200,
            callback: () => {
                this.move();
            },
        });
    }

    update() {}
}
