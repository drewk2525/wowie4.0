export default class StatBar {
    background: Phaser.GameObjects.Rectangle;
    bar: Phaser.GameObjects.Rectangle;
    value: number;
    maxValue: number;
    fullAlpha: number;
    alpha: number;

    constructor(scene: Phaser.Scene, config: statBarConfig) {
        const {
            x = 0,
            y = -50,
            width = 64,
            height = 8,
            color = 0xcc3333,
            backgroundColor = 0x662222,
            fullAlpha = 0,
            alpha = 1,
            borderSize = 4,
            value,
            maxValue,
        } = config;
        this.value = value;
        this.maxValue = maxValue;
        this.fullAlpha = fullAlpha;
        this.alpha = alpha;

        this.bar = scene.add.rectangle(
            x,
            y,
            width - borderSize,
            height - borderSize,
            color,
            fullAlpha
        );
        this.bar.setDepth(100);

        this.background = scene.add.rectangle(
            x,
            y,
            width,
            height,
            backgroundColor,
            fullAlpha
        );
        this.bar.setDepth(101);
        this.update(this.value);
    }

    update(newValue, newMax?) {
        this.value = newValue >= 0 ? newValue : 0;
        this.maxValue = newMax || this.maxValue;
        this.bar.width = (this.value * this.background.width) / this.maxValue;
        if (this.value < this.maxValue) {
            this.background.fillAlpha = this.alpha;
            this.bar.fillAlpha = this.alpha;
        } else {
            this.background.fillAlpha = this.fullAlpha;
            this.bar.fillAlpha = this.fullAlpha;
        }
    }

    setColor(color = 0xcc3333, backgroundColor = 0x662222) {
        this.bar.fillColor = color;
        this.background.fillColor = backgroundColor;
    }
}
