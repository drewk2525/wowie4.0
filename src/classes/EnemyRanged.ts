import BattleScene from "../scenes/BattleScene";
import Enemy from "./Enemy";
import { enemyAbilities } from "../utils/enemyAbilities";
import AllyAbility from "./AllyAbility";
const rng = new Phaser.Math.RandomDataGenerator();

export default class EnemyRanged extends Enemy {
    displayWidth: number;
    displayHeight: number;
    range: integer;
    ability: AllyAbility;

    constructor(scene: BattleScene, name: string, unitConfig: unitConfig) {
        super(scene, name, unitConfig);
        this.displayWidth = 70;
        this.displayHeight = 70;
        this.body.collideWorldBounds = false;

        this.scene.time.addEvent({
            delay: 200,
            callback: () => {
                if (this.isAlive() && scene.player.isAlive()) {
                    this.move();
                }
            },
        });
        setTimeout(() => {
            const taser = enemyAbilities[0];
            taser.myself = this;
            this.ability = new AllyAbility(this.scene, taser);
        }, 200);
    }

    attemptAction() {
        if (
            this.inBounds() &&
            this.ability &&
            Phaser.Math.Distance.BetweenPoints(
                this.container,
                this.scene.player.container
            ) <= this.ability.range
        ) {
            this.ability.proc();
        }
    }

    update() {
        this.attemptAction();
    }

    stopMoving() {
        // @ts-ignore
        this.container.body.setVelocityX(0);
        // @ts-ignore
        this.container.body.setVelocityY(0);
    }
}
