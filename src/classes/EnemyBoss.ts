import BattleScene from "../scenes/BattleScene";
import { bossCollectables, collectables } from "../utils/data";
import Collectable from "./Collectable";
import Enemy from "./Enemy";
const rng = new Phaser.Math.RandomDataGenerator();

export default class EnemyBoss extends Enemy {
    displayWidth: number;
    displayHeight: number;
    aggroCircle: Phaser.GameObjects.Arc;
    charging: boolean;
    chargeOnCooldown: boolean;

    constructor(scene: BattleScene, name: string, unitConfig: unitConfig) {
        super(scene, name, unitConfig);
        this.displayWidth = 70;
        this.displayHeight = 70;
        this.isBoss = true;
        this.chargeOnCooldown = true;
        this.chargeCooldown();
        this.body.collideWorldBounds = false;
        this.mainObj.setScale(this.spriteScale);

        const hitSfx = [
            "robot_bossSpawn1",
            "robot_bossSpawn2",
            "robot_bossSpawn3",
        ];
        this.scene.sfx[hitSfx[rng.between(0, hitSfx.length - 1)]]();

        this.scene.time.addEvent({
            delay: 200,
            callback: () => {
                if (this.isAlive() && scene.player.isAlive()) {
                    this.move();
                }
            },
        });
    }

    attemptAction() {
        if (this.charging || this.chargeOnCooldown) {
            return;
        }
        const readyToUse =
            this.inBounds() &&
            Phaser.Math.Distance.BetweenPoints(
                this.container,
                this.scene.player.container
            ) <= 600;
        if (readyToUse) {
            this.charging = true;
            this.chargeOnCooldown = true;
            // @ts-ignore
            this.container.body.setVelocityX(0);
            // @ts-ignore
            this.container.body.setVelocityY(0);
            this.scene.sfx.robot_bossCharge();
            this.scene.add.tween({
                targets: this.container,
                x: this.container.x + 20,
                duration: 50,
                repeat: 10,
                yoyo: true,
                onComplete: () => {
                    if (!this.isAlive() || !this.scene.player.isAlive()) {
                        return;
                    }
                    if (readyToUse) {
                        this.scene.physics.moveToObject(
                            this.container,
                            this.scene.player.container,
                            800
                        );
                        this.scene.time.addEvent({
                            delay: 700,
                            callback: () => {
                                if (!this.container.body) {
                                    return;
                                }
                                // @ts-ignore
                                this.container.body.setVelocityX(0);
                                // @ts-ignore
                                this.container.body.setVelocityY(0);
                                this.charging = false;
                            },
                        });
                    } else {
                        this.charging = false;
                        this.move();
                    }
                },
            });
            this.chargeCooldown();
        }
    }

    chargeCooldown() {
        this.scene.time.addEvent({
            delay: 6000,
            callback: () => {
                this.chargeOnCooldown = false;
            },
        });
    }

    pulse() {
        this.scene.add.tween({
            targets: this.mainObj,
            scaleX: this.spriteScale * 1.1,
            scaleY: this.spriteScale * 0.9,
            duration: 500,
            repeat: -1,
            yoyo: true,
        });
    }

    die() {
        this.scene.bossesKilled++;
        this.scene.bossesKilledText.text = `Bosses killed: ${this.scene.bossesKilled}/${this.scene.bossesKilledMax}`;
        if (this.scene.bossesKilled === this.scene.bossesKilledMax) {
            this.scene.scene.launch("gameOver", { win: true });
            this.scene.scene.pause();
        }
        let drop = bossCollectables[0];
        if (this.scene.bossesKilled > 2 && this.scene.bossesKilled < 5) {
            drop = bossCollectables[1];
        } else if (this.scene.bossesKilled == 5) {
            drop = bossCollectables[2];
        } else if (this.scene.bossesKilled > 5) {
            drop = collectables[rng.between(0, 3)];
        }
        const collectable = new Collectable(this.scene, drop, {
            x: this.container.x,
            y: this.container.y,
        });
        this.scene.collectableGroup.add(collectable.container);
        super.die(true);
    }

    move() {
        const scene = this.scene;
        if (!this.isAlive() || !scene.player.isAlive() || this.charging) {
            return;
        }
        scene.physics.moveToObject(
            this.container,
            scene.player.container,
            this.moveSpeed
        );
        this.state = "moving";
    }

    update() {
        this.attemptAction();
        this.move();
    }
}
