import BattleScene from "../scenes/BattleScene";
import Unit from "./Unit";

export default class Player extends Unit {
    constructor(scene: BattleScene, name: string, unitConfig: unitConfig) {
        super(scene, name, unitConfig);
        this.hitpointBar.setColor(0x33cc33, 0x226622);
        this.hitSfx = ["player_hit1", "player_hit2", "player_hit3"];
    }

    die() {
        // @ts-ignore
        this.mainObj.fillColor = 0xff6666;
        this.body.enable = false;
        this.state = "dead";
        this.mainObj.play({
            key: `${this.name}_death`,
            frameRate: 6,
            repeat: 0,
        });
        this.scene.scene.launch("gameOver", { win: false });
    }

    move(direction: string, dirModifier: coordinates, speed?: number): void {
        if (this.scene.moveTutorialTip) {
            // @ts-ignore
            this.scene.moveTutorialTip.targets[0].destroy();
            this.scene.moveTutorialTip.complete();
        }
        super.move(direction, dirModifier, speed);
    }

    heal() {
        this.takeDamage(-3);
        const healSprite = this.scene.add.sprite(0, 0, "healPlayer");
        healSprite.play("healPlayer");
        this.container.add(healSprite);
        this.scene.time.addEvent({
            delay: 1000,
            callback: () => {
                healSprite.destroy();
            },
        });
    }

    pulse() {
        // Don't pulse
    }

    update() {
        const scene = this.scene;
        let keyDown = false;
        let coord: coordinates = { x: 0, y: 0 };
        let direction = "";
        // @ts-ignore - Up
        if (scene.keys.up.isDown || scene.keys.up2.isDown) {
            direction = "Up";
            coord.y = -1;
            keyDown = true;
        }

        // @ts-ignore - Down
        if (scene.keys.down.isDown || scene.keys.down2.isDown) {
            direction = "Down";
            coord.y = 1;
            keyDown = true;
        }

        // @ts-ignore - Left
        if (scene.keys.left.isDown || scene.keys.left2.isDown) {
            direction = "Left";
            coord.x = -1;
            keyDown = true;
        }

        // @ts-ignore - Right
        if (scene.keys.right.isDown || scene.keys.right2.isDown) {
            direction = "Right";
            coord.x = 1;
            keyDown = true;
        }

        if (keyDown) {
            this.move(direction, coord);
        } else if (!this.isBusy() && this.isAlive()) {
            this.state = "idle";
            this.idle();
        }
    }
}
