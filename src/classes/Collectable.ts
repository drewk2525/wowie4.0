import BattleScene from "../scenes/BattleScene";
import { bossCollectables } from "../utils/data";

const rng = new Phaser.Math.RandomDataGenerator();

export default class Collectable {
    image: string;
    scene: BattleScene;
    sprite: Phaser.GameObjects.Sprite;
    aggroCircle: Phaser.GameObjects.Arc;
    highlightCircle: Phaser.GameObjects.Arc;
    moveSpeed: number;
    container: Phaser.GameObjects.Container;
    sfx: string;
    state: string;

    constructor(scene, image, coord: coordinates) {
        this.moveSpeed = 600;
        this.sfx = "collectScrap";
        this.state = "idle";
        this.image = image;
        this.scene = scene;
        this.container = scene.add.container(coord.x, coord.y);

        // @ts-ignore
        if (bossCollectables.includes(image)) {
            this.highlightCircle = scene.add.circle(0, 0, 40, 0xf28604);
            this.scene.physics.world.enableBody(this.highlightCircle);
            this.highlightCircle.setAlpha(0.1);
            this.container.add(this.highlightCircle);
        }

        this.sprite = scene.add.sprite(0, 0, "spareParts");
        this.container.add(this.sprite);
        this.aggroCircle = scene.add.circle(0, 0, 180, 0xffff99);
        this.scene.physics.world.enableBody(this.aggroCircle);
        const aggroAlpha = this.scene.physics.config.debug ? 0.3 : 0;
        this.aggroCircle.setAlpha(aggroAlpha);
        this.container.add(this.aggroCircle);
        this.container.setData({ self: this });
        scene.physics.world.enableBody(this.container);
        // @ts-ignore
        this.container.body.setOffset(
            -this.sprite.displayWidth / 2,
            -this.sprite.displayHeight / 2
        );
        const scale = 0.7;
        this.sprite.setScale(scale);
        this.sprite.play(image);

        scene.add.tween({
            targets: this.container,
            scaleX: scale * 1.2,
            scaleY: scale * 1.2,
            duration: 500,
            yoyo: true,
            repeat: -1,
        });
    }

    collect() {
        const scene = this.scene;
        scene.sfx[this.sfx]();
        const textProps = {
            fontFamily: "sans-serif",
            align: "center",
            fontSize: 40,
            color: "#bb9966",
        };
        let drop = this.image.slice(3);
        // @ts-ignore
        drop = bossCollectables.includes(this.image) ? drop : "scrap";
        const text = scene.add.text(
            this.container.x,
            this.container.y,
            `+1 ${drop}`,
            // @ts-ignore
            textProps
        );
        scene.add.tween({
            targets: text,
            scaleX: 1.2,
            scaleY: 1.2,
            duration: 500,
            ease: "Quint.easeIn",
            y: this.container.y - 50,
            alpha: 0.3,
            yoyo: false,
            repeat: 0,
            onComplete: () => {
                text.destroy();
            },
        });
        scene.addResource(drop);
        this.container.destroy();
    }

    update() {
        if (!this.scene.player.isAlive()) {
            this.stopMoving();
            return;
        }

        const overlaps = this.scene.physics.overlap(
            this.aggroCircle,
            this.scene.player.container,
            (circle, player) => {}
        );
        if (overlaps) {
            this.scene.physics.moveToObject(
                this.container,
                this.scene.player.container,
                this.moveSpeed
            );
            if (this.state !== "moving") {
                this.state = "moving";
            }
        } else {
            this.stopMoving();
        }
    }

    stopMoving() {
        // @ts-ignore
        this.container.body.setVelocityX(0);
        // @ts-ignore
        this.container.body.setVelocityY(0);
        this.state = "idle";
    }
}
