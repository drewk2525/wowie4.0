import { Scene } from "phaser";
import BattleScene from "../scenes/BattleScene";
import Enemy from "./Enemy";
import EnemyRanged from "./EnemyRanged";
import EnemyBoss from "./EnemyBoss";
const rng = new Phaser.Math.RandomDataGenerator();

export default class EnemySpawner {
    scene: BattleScene;
    group: Phaser.GameObjects.Group;
    spawnTimer: number;
    spawnTimerMin: number;
    spawnCounter: number;
    bossSpawnCounterCur: number;
    bossSpawnCounterMin: number;

    constructor(scene: BattleScene) {
        this.spawnTimer = 5000;
        this.spawnTimerMin = 1000;
        this.spawnCounter = 0;
        this.bossSpawnCounterMin = 15;
        this.bossSpawnCounterCur = 25;
        this.scene = scene;
        this.group = scene.physics.add.group();
        this.spawnEnemy();
        this.scene.time.addEvent({
            delay: 5000,
            callback: () => {
                this.spawnTimer = Phaser.Math.Clamp(
                    (this.spawnTimer -= 25),
                    this.spawnTimerMin,
                    this.spawnTimer
                );
            },
            repeat: -1,
        });
        this.spawnEnemy();
    }

    spawnEnemy() {
        this.spawnCounter++;
        const scene = this.scene;
        const angle = Math.random() * Math.PI * 2;
        const radius = 1300;
        const x = Math.cos(angle) * radius + scene.scale.width / 2;
        const y = Math.sin(angle) * radius + scene.scale.height / 2;
        const enemyTypes = [
            {
                class: Enemy,
                type: "robot_melee",
                unitConfig: {
                    moveSpeed: 175,
                    attackDamage: 5,
                    maxHitpoints: 12,
                },
                chance: 10,
            },
            {
                class: EnemyRanged,
                type: "robot_ranged",
                unitConfig: {
                    moveSpeed: 50,
                    attackDamage: 5,
                    maxHitpoints: 8,
                },
                chance: 2,
            },
        ];
        const bossTypes = [
            {
                class: EnemyBoss,
                type: "robot_boss",
                unitConfig: {
                    moveSpeed: 90,
                    attackDamage: 10,
                    maxHitpoints: 25,
                    spriteScale: 2.5,
                },
                chance: 0,
            },
        ];

        const randMax = enemyTypes.reduce((acc, enemy) => {
            return acc + enemy.chance;
        }, 0);
        const randNum = rng.between(1, randMax);
        let i = 0;
        let enemyType = enemyTypes.find((enemy) => {
            i += enemy.chance;
            return randNum <= i;
        });
        if (
            this.spawnCounter % this.bossSpawnCounterCur === 0 &&
            this.spawnCounter > 0
        ) {
            this.bossSpawnCounterCur = Phaser.Math.Clamp(
                (this.bossSpawnCounterCur -= 1),
                this.bossSpawnCounterMin,
                this.bossSpawnCounterCur
            );
            enemyType = bossTypes[0];
            enemyType.unitConfig.maxHitpoints +=
                (Math.abs(this.bossSpawnCounterCur - 25) /
                    this.bossSpawnCounterMin) *
                120;
            this.spawnTimer = Phaser.Math.Clamp(
                (this.spawnTimer -= 180),
                this.spawnTimerMin,
                this.spawnTimer
            );
        }
        const newEnemy = new enemyType.class(scene, enemyType.type, {
            x,
            y,
            ...enemyType.unitConfig,
        });
        this.group.add(newEnemy.container);
        this.scene.time.addEvent({
            delay: this.spawnTimer,
            callback: () => {
                this.spawnEnemy();
            },
        });
    }

    update() {}
}
