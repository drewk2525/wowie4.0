import BattleScene from "../scenes/BattleScene";
import StatBar from "./StatBar";

const rng = new Phaser.Math.RandomDataGenerator();

export default class Unit {
    hitpoints: integer;
    maxHitpoints: integer;
    spriteName: string;
    mainObj: Phaser.GameObjects.Sprite;
    container: Phaser.GameObjects.Container;
    innerContainer: Phaser.GameObjects.Container;
    body: Phaser.Physics.Arcade.Body;
    state: string;
    moveSpeed: integer;
    maxMoveSpeed: integer;
    isPushable: boolean;
    direction: string;
    scene: BattleScene;
    invincible: boolean;
    attackDamage: number;
    name: string;
    spriteScale: number;
    hitpointBar: StatBar;
    hitpointBarConfig: statBarConfig;
    hitStunTime: number;
    hitSfx: Array<string>;
    target?: Phaser.GameObjects.Container;
    isBoss?: boolean;
    isEnemy?: boolean;

    constructor(scene: BattleScene, name, unitConfig: unitConfig) {
        const { x, y } = unitConfig;
        this.state = "idle";
        this.scene = scene;
        this.invincible = false;
        this.name = name;
        // @ts-ignore
        this.target = unitConfig.target;
        this.spriteScale = unitConfig.spriteScale || 1.6;
        this.moveSpeed = unitConfig.moveSpeed || 100;
        this.maxMoveSpeed = this.moveSpeed;
        this.hitStunTime = unitConfig.hitStunTime || 100;
        this.attackDamage = unitConfig.attackDamage;
        this.maxHitpoints = unitConfig.maxHitpoints;
        this.hitpoints = this.maxHitpoints;
        this.isPushable = true;

        // main sprite
        this.mainObj = scene.add.sprite(0, 0, name);
        this.mainObj.setScale(this.spriteScale);

        this.container = scene.add.container(x, y);
        this.innerContainer = scene.add.container(0, 0);
        this.innerContainer.add(this.mainObj);
        this.container.add(this.innerContainer);
        scene.physics.world.enableBody(this.container);

        // @ts-ignore - IDE isn't smart enough to know that enableBody alters this.body
        this.body = this.container.body;
        // @ts-ignore - go home TS, you're drunk
        const w = this.displayWidth || this.mainObj.displayWidth;
        // @ts-ignore
        const h = this.displayHeight || this.mainObj.displayHeight;
        const offset = unitConfig.bodyOffset || 40;
        this.body.setSize(w - offset, h - offset);
        this.body.setOffset(-(w - offset) / 2, -(h - offset) / 2);
        this.body.collideWorldBounds = true;
        this.body.customBoundsRectangle = scene.customBounds;
        this.scene.time.addEvent({
            delay: 200,
            callback: () => {
                this.body.setImmovable(true);
            },
        });
        this.container.setData({ self: this });
        this.hitpointBarConfig = {
            value: this.hitpoints,
            maxValue: this.maxHitpoints,
        };
        this.hitpointBar = new StatBar(scene, this.hitpointBarConfig);
        this.container.add(this.hitpointBar.background);
        this.container.add(this.hitpointBar.bar);
        this.pulse();
        this.idle();
    }

    takeDamage(damage: number) {
        if (this.invincible && damage > 0) {
            return;
        }
        if (this.hitSfx && damage > 0) {
            this.scene.sfx[
                this.hitSfx[rng.between(0, this.hitSfx.length - 1)]
            ]();
        }
        this.invincible = true;
        this.scene.time.addEvent({
            delay: this.hitStunTime,
            callback: () => {
                this.invincible = false;
            },
        });

        let damageMod = 1;
        const maxDistance = 300;
        const maxMod = 2.5;
        if (this.isEnemy && this.scene.ally.bruteForce) {
            const dist = Phaser.Math.Distance.Between(
                this.container.x,
                this.container.y,
                this.scene.ally.container.x,
                this.scene.ally.container.y
            );
            damageMod = 1 / (dist / (maxDistance * 0.8));
            if (damageMod > maxMod) {
                damageMod = maxMod;
            }
            if (damageMod < 1) {
                damageMod = 1;
            }
        }
        this.hitpoints -= damage * damageMod;
        if (this.hitpoints > this.maxHitpoints) {
            this.hitpoints = this.maxHitpoints;
        }
        this.hitpointBar.update(this.hitpoints);

        if (this.hitpoints <= 0) {
            this.die(false);
        } else if (damage > 0) {
            this.hitFlash();
        }
    }

    hitFlash() {
        //temp hack until we have sprites
        this.scene.tweens.addCounter({
            from: 0.3,
            to: 0.8,
            duration: 250,
            repeat: 2,
            onUpdate: (tween) => {
                const value = tween.getValue();
                // @ts-ignore
                this.mainObj.alpha = value;
            },
            onComplete: () => {
                // @ts-ignore
                this.mainObj.alpha = 1;
            },
        });
    }

    idle() {
        if (this.isBusy() || !this.isAlive()) {
            return;
        }
        if (
            !this.mainObj.anims.currentAnim ||
            this.mainObj.anims.currentAnim.key !== `${this.name}_idle`
        ) {
            // @ts-ignore
            if (this.scene.anims.anims.entries[`${this.name}_idle`]) {
                this.mainObj.play({
                    key: `${this.name}_idle`,
                    frameRate: 6,
                    repeat: -1,
                });
            }
        }
        this.body.setVelocityX(0);
        this.body.setVelocityY(0);
    }

    move(_direction: string, dirModifier: coordinates, speed?: number) {
        if (this.isBusy() || !this.isAlive()) {
            return;
        }
        if (
            !this.mainObj.anims.currentAnim ||
            this.mainObj.anims.currentAnim.key !== `${this.name}_walking`
        ) {
            this.mainObj.play({
                key: `${this.name}_walking`,
                frameRate: 12,
                repeat: -1,
            });
        }
        if (dirModifier.x < 0) {
            this.mainObj.flipX = true;
        } else {
            this.mainObj.flipX = false;
        }
        // this.state = "moving";
        this.body.setVelocityX(speed || this.moveSpeed * dirModifier.x);
        this.body.setVelocityY(speed || this.moveSpeed * dirModifier.y);
    }

    die(_isDead) {
        this.state = "dead";
        this.container.destroy();
    }

    isBusy() {
        return this.state !== "idle" && this.state !== "moving";
    }

    isAlive() {
        return this.state !== "dead";
    }

    pulse() {
        this.scene.add.tween({
            targets: this.mainObj,
            scaleX: this.spriteScale * 1.1,
            scaleY: this.spriteScale * 0.9,
            duration: 500,
            repeat: -1,
            yoyo: true,
        });
    }

    damageBounceBack(attacker) {
        if (!this.isPushable) {
            return;
        }
        this.isPushable = false;
        const t = this.container;
        const a = attacker;
        let x = 0,
            y = 0;
        if (t.y >= a.y) {
            y = 1;
        }
        if (t.y < a.y) {
            y = -1;
        }
        if (t.x >= a.x) {
            x = 1;
        }
        if (t.x < a.x) {
            x = -1;
        }
        const distance = this.moveSpeed < this.maxMoveSpeed ? 10 : 30;
        this.scene.add.tween({
            targets: this.container,
            x: this.container.x + distance * x,
            y: this.container.y + distance * y,
            duration: 100,
            repeat: 0,
            onComplete: () => {
                this.body.setVelocityX(0);
                this.body.setVelocityY(0);
            },
        });
        this.scene.time.addEvent({
            delay: 150,
            callback: () => {
                this.isPushable = true;
            },
        });
    }

    getXyDirction(direction: string) {
        let x = 0;
        let y = 0;
        if (direction === "Down") {
            y = 1;
        }
        if (direction === "Right") {
            x = 1;
        }
        if (direction === "Up") {
            y = -1;
        }
        if (direction === "Left") {
            x = -1;
        }
        return { x, y };
    }
}
