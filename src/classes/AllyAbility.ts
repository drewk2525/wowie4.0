import BattleScene from "../scenes/BattleScene";
import Unit from "./Unit";

export default class AllyAbility {
    range?: number;
    attackSpeed?: number;
    spriteScale?: number;
    projectiles?: number;
    upgradeSpriteScale?: number;
    area?: boolean;
    callback?: Function;
    unlocked?: boolean;
    isEnemy?: boolean;
    myself?: Unit;
    scene: BattleScene;
    projectileSprite?: string;
    projectileName: string;
    projectileSpeed: number;
    projectileDamage: number;
    projectileMaxTargets: number;
    spread: boolean;
    sfx: string;
    onCooldown: boolean;
    projectileDelay: number;
    upgradeSprite: string;
    upgradePosition: coordinates;
    name: string;

    constructor(scene: BattleScene, abilityStats: ability) {
        this.scene = scene;
        this.range = abilityStats.range;
        this.attackSpeed = abilityStats.attackSpeed;
        this.spriteScale = abilityStats.spriteScale;
        this.projectiles = abilityStats.projectiles;
        this.upgradeSpriteScale = abilityStats.upgradeSpriteScale;
        this.area = abilityStats.area;
        this.callback = abilityStats.callback;
        this.unlocked = abilityStats.unlocked;
        this.isEnemy = abilityStats.isEnemy;
        this.spread = abilityStats.spread;
        this.projectileSprite = abilityStats.projectileSprite;
        // @ts-ignore
        this.myself = this.isEnemy ? abilityStats.myself : scene.ally;
        this.projectileName = abilityStats.projectileName;
        this.projectileSpeed = abilityStats.projectileSpeed;
        this.projectileDamage = abilityStats.projectileDamage;
        this.projectileMaxTargets = abilityStats.projectileMaxTargets;
        this.sfx = abilityStats.sfx;
        this.projectileDelay = abilityStats.projectileDelay;
        this.name = abilityStats.name;
        this.upgradeSprite = abilityStats.upgradeSprite;
        this.upgradePosition = abilityStats.upgradePosition;
        this.onCooldown = false;
    }

    proc() {
        if (this.onCooldown) {
            return;
        }
        const scene = this.scene;
        if (this.callback) {
            this.onCooldown = true;
            this.callback(scene);
            this.cooldownTimer();
        }
        if (this.range) {
            if (
                this.range <
                Phaser.Math.Distance.BetweenPoints(
                    this.myself.container,
                    this.myself.target
                )
            ) {
                return;
            }
            this.onCooldown = true;
            const projectiles = this.projectiles || 1;
            for (let i = 0; i < projectiles; i++) {
                setTimeout(
                    () => {
                        if (this.sfx) {
                            scene.sfx[this.sfx]();
                        }
                        const projSprite =
                            this.projectileSprite || "smallAbilities";
                        const projectile = scene.add.sprite(
                            this.myself.container.x,
                            this.myself.container.y,
                            projSprite
                        );
                        projectile.setScale(this.spriteScale);
                        projectile.setData({
                            damage: this.projectileDamage,
                            targetsRemaining: this.projectileMaxTargets,
                            maxDistance: this.range,
                            distanceTraveled: 0,
                        });
                        projectile.play({
                            key: this.projectileName,
                            frameRate: 12,
                            repeat: -1,
                        });
                        if (this.isEnemy) {
                            scene.enemyProjectileGroup.add(projectile);
                        } else {
                            scene.allyProjectileGroup.add(projectile);
                        }
                        this.scene.time.addEvent({
                            delay: 2,
                            callback: () => {
                                // @ts-ignore
                                projectile.body.setImmovable(true);
                            },
                        });
                        // @ts-ignore
                        const body: Phaser.Physics.Arcade.Body =
                            projectile.body;

                        // Angle for spread / shotgun
                        const angleBetween = Phaser.Math.Angle.BetweenPoints(
                            this.myself.container,
                            this.myself.target
                        );
                        let angle;
                        const offset = 0.15;

                        if ((this.spread && i === 0) || !this.spread) {
                            angle = angleBetween;
                            // scene.physics.moveToObject(
                            //     projectile,
                            //     this.myself.target,
                            //     this.projectileSpeed
                            // );
                        } else if (i % 2 === 0) {
                            angle = angleBetween - offset * (i / 2);
                        } else {
                            angle = angleBetween - offset * ((i + 1) / 2);
                        }
                        const xVel = this.projectileSpeed * Math.cos(angle);
                        const yVel = this.projectileSpeed * Math.sin(angle);

                        // @ts-ignore
                        projectile.body.setVelocityX(xVel);
                        // @ts-ignore
                        projectile.body.setVelocityY(yVel);

                        if (this.spread && i === 0) {
                            const sprite = scene.add.sprite(
                                0,
                                0,
                                "smallAbilities"
                            );
                            sprite.play({
                                key: "smoke",
                                frameRate: 12,
                                repeat: 0,
                                hideOnComplete: true,
                            });
                            const velMod = 8;
                            scene.physics.world.enableBody(sprite);
                            // @ts-ignore
                            sprite.body.setVelocityX(xVel / velMod);
                            // @ts-ignore
                            sprite.body.setVelocityY(yVel / velMod);

                            sprite.setScale(2);
                            scene.ally.container.add(sprite);
                            scene.time.addEvent({
                                delay: this.attackSpeed,
                                callback: () => {
                                    sprite.destroy();
                                },
                            });
                        }

                        scene.time.addEvent({
                            delay: 20,
                            callback: () => {
                                const angle =
                                    (projectile.body &&
                                        // @ts-ignore
                                        projectile.body.angle) ||
                                    0;
                                projectile.setAngle(
                                    Phaser.Math.RadToDeg(angle) - 90
                                );
                            },
                        });
                    },
                    this.spread ? 0 : i * this.projectileDelay
                );
            }
            this.cooldownTimer();
        }
    }

    cooldownTimer() {
        const delay = this.isEnemy
            ? this.attackSpeed
            : this.attackSpeed * this.scene.ally.baseAbilitySpeed;
        this.scene.time.addEvent({
            delay: delay,
            callback: () => {
                this.onCooldown = false;
            },
        });
    }
}
