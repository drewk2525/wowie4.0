import { isConstructorDeclaration } from "typescript";
import BattleScene from "../scenes/BattleScene";
import { allAbilities } from "../utils/allyAbilities";
import AllyAbility from "./AllyAbility";
import Unit from "./Unit";
const rng = new Phaser.Math.RandomDataGenerator();

export default class Ally extends Unit {
    aggroCircle: Phaser.GameObjects.Arc;
    wanderTimer: Phaser.Time.TimerEvent;
    baseRange: number;
    baseAbilitySpeed: number;
    abilities: Array<AllyAbility>;
    wanderDirection: number;
    passiveCounts: object;
    innerContainer: Phaser.GameObjects.Container;
    bruteForce: boolean;

    constructor(scene: BattleScene, name: string, unitConfig: unitConfig) {
        super(scene, name, unitConfig);
        this.baseRange = 10;
        this.baseAbilitySpeed = 1;
        this.bruteForce = false;
        this.aggroCircle = scene.add.circle(0, 0, 300);
        this.scene.physics.world.enableBody(this.aggroCircle);
        this.passiveCounts = {};

        // const aggroAlpha = this.scene.physics.config.debug ? 0.1 : 0;
        this.aggroCircle.setStrokeStyle(2, 0xff3333);
        this.aggroCircle.setAlpha(0.6);
        this.container.add(this.aggroCircle);
        this.wander();

        this.scene.time.addEvent({
            delay: 200,
            callback: () => {
                this.abilities = allAbilities.reduce((acc, ability) => {
                    acc.push(new AllyAbility(this.scene, ability));
                    return acc;
                }, []);
            },
        });
    }

    attemptAction() {
        if (this.target && this.target.data) {
            this.state = "attack";
            this.abilities.forEach((ability) => {
                if (ability.unlocked) {
                    ability.proc();
                }
            });
            if (
                Phaser.Math.Distance.BetweenPoints(
                    this.container,
                    this.target
                ) > 100
            ) {
                this.scene.physics.moveToObject(
                    this.container,
                    this.target,
                    this.moveSpeed * 2
                );
            }
        } else if (this.state === "attack") {
            this.state = "idle";
            this.idle();
            if (this.wanderTimer.hasDispatched) {
                this.wander();
            }
        }
    }

    getAbilityByName(name) {
        return this.abilities.find((ability) => ability.name === name);
    }

    isAbilityUnlocked(a) {
        return (
            this.abilities.findIndex(
                (ability) => ability.name === a.name && ability.unlocked
            ) > -1
        );
    }

    addAbility(a) {
        this.abilities.forEach((ability) => {
            if (ability.name === a.name) {
                ability.unlocked = true;
                this.addUpgradeSprite(ability);
            }
        });
        if (a.infiniteBuys) {
            this.passiveCounts[a.name] = this.passiveCounts[a.name]
                ? this.passiveCounts[a.name] + 1
                : 1;
        }
    }

    addUpgradeSprite(ability) {
        const { x, y } = ability.upgradePosition;
        const upgradeSprite = this.scene.add.sprite(
            x,
            y,
            ability.upgradeSprite
        );
        upgradeSprite.setScale(ability.upgradeSpriteScale || 0.5);
        upgradeSprite.play({ key: ability.upgradeSprite.slice(3), repeat: -1 });
        this.innerContainer.add(upgradeSprite);
    }

    wander() {
        if (this.isBusy()) {
            return;
        }
        const locations = [
            { x: 650, y: 280 },
            { x: 650, y: 620 },
            { x: 1260, y: 280 },
            { x: 1260, y: 620 },
        ];
        if (!this.wanderDirection) {
            this.wanderDirection = 0;
        }
        const location = locations[this.wanderDirection];
        this.wanderDirection++;
        if (this.wanderDirection > locations.length - 1) {
            this.wanderDirection = 0;
        }
        this.scene.physics.moveTo(this.container, location.x, location.y, 50);
        this.wanderTimer = this.scene.time.addEvent({
            delay: 4000,
            callback: () => {
                this.idle();
                this.wanderTimer = this.scene.time.addEvent({
                    delay: 1000,
                    callback: () => {
                        this.wander();
                    },
                });
            },
        });
    }

    slowEnemiesInRange() {
        this.scene.physics.overlap(
            this.aggroCircle,
            this.scene.enemySpawner.group,
            (ally, enemy) => {
                enemy.data.values.self.slow();
            }
        );
    }

    healPlayerInRange() {
        const overlaps = this.scene.physics.overlap(
            this.aggroCircle,
            this.scene.player.container
        );
        if (overlaps) {
            this.scene.player.heal();
        }
        const healSprite = this.scene.add.sprite(0, 0, "healPlayer");
        healSprite.play("healPlayer");
        this.container.add(healSprite);
        this.scene.time.addEvent({
            delay: 1000,
            callback: () => {
                healSprite.destroy();
            },
        });
    }

    staticFieldDamage(damage) {
        this.scene.physics.overlap(
            this.aggroCircle,
            this.scene.enemySpawner.group,
            (ally, enemy) => {
                enemy.data.values.self.staticZap();
                enemy.data.values.self.takeDamage(damage);
            }
        );
    }

    // getClosestEnemy() {
    //     return this.scene.enemySpawner.group
    //         .getChildren()
    //         .sort(
    //             (a, b) =>
    //                 Phaser.Math.Distance.BetweenPoints(
    //                     this.container,
    //                     a.container
    //                 ) -
    //                 Phaser.Math.Distance.BetweenPoints(
    //                     this.container,
    //                     b.container
    //                 )
    //         )[0];
    // }

    pulse() {
        this.scene.add.tween({
            targets: this.innerContainer,
            scaleX: 1.1,
            scaleY: 0.9,
            duration: 500,
            repeat: -1,
            yoyo: true,
        });
    }

    update() {
        this.attemptAction();

        // unslow enemies not in range
        this.scene.enemySpawner.group.getChildren().forEach((e) => {
            const overlaps = this.scene.physics.overlap(this.aggroCircle, e);
            const enemy = e.data.values.self;
            if (!overlaps && enemy.moveSpeed !== enemy.originalMoveSpeed) {
                this.scene.time.addEvent({
                    delay: 2000,
                    callback: () => {
                        enemy.unslow();
                    },
                });
            }
        });

        const overlaps = this.scene.physics.overlap(
            this.aggroCircle,
            this.scene.enemySpawner.group,
            (ally, e) => {}
        );
        if (overlaps) {
            this.aggroCircle.strokeColor = 0xff3333;
            // @ts-ignore
            const closestEnemy: Phaser.GameObjects.Container =
                this.scene.enemySpawner.group
                    .getChildren()
                    .filter((child) => {
                        return child.data.values.self.inBounds();
                    })
                    .sort(
                        (
                            a: Phaser.GameObjects.Container,
                            b: Phaser.GameObjects.Container
                        ) =>
                            Math.abs(
                                Phaser.Math.Distance.Between(
                                    this.container.x,
                                    this.container.y,
                                    a.x,
                                    a.y
                                )
                            ) -
                            Math.abs(
                                Phaser.Math.Distance.Between(
                                    this.container.x,
                                    this.container.y,
                                    b.x,
                                    b.y
                                )
                            )
                    )[0];
            this.target = closestEnemy;
        } else {
            this.aggroCircle.strokeColor = 0xffff00;
        }
    }
}
