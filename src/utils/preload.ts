import BattleScene from "../scenes/BattleScene";

export const preload = (scene) => {
    const spriteConfig = {
        frameWidth: 64,
        frameHeight: 64,
        endFrame: 0,
    };

    // Static images
    scene.load.image("background", "background.png");
    scene.load.image(
        "upgradeScreenBackground",
        "UpgradeScreen_files/UpgradeBackground.png"
    );
    scene.load.image("upgradeTierBorder", "UpgradeScreen_files/tierBorder.png");
    scene.load.image("battery", "static/spr_battery.png");
    scene.load.image("cpu", "static/spr_cpu.png");
    scene.load.image("flashDrive", "static/spr_flashDrive.png");
    scene.load.image("ram", "static/spr_ram.png");
    scene.load.image("icon_spareParts", "static/spr_icon_spareParts.png");
    scene.load.image("arrow", "static/arrow.png");

    // Spritesheets
    scene.load.spritesheet(
        "robot_melee",
        "sprites/spr_robot_melee.png",
        spriteConfig
    );
    scene.load.spritesheet(
        "robot_ranged",
        "sprites/spr_robot_ranged.png",
        spriteConfig
    );
    scene.load.spritesheet(
        "robot_boss",
        "sprites/spr_robot_boss.png",
        spriteConfig
    );
    scene.load.aseprite(
        "smallAbilities",
        "sprites/smallAbilities.png",
        "sprites/smallAbilities.json"
    );
    scene.load.aseprite(
        "laserBeam",
        "sprites/laserBeam.png",
        "sprites/laserBeam.json"
    );
    scene.load.aseprite("player", "sprites/player.png", "sprites/player.json");
    scene.load.aseprite(
        "spareParts",
        "sprites/spareParts.png",
        "sprites/spareParts.json"
    );
    scene.load.aseprite("ally", "sprites/ally.png", "sprites/ally.json");
    scene.load.aseprite(
        "btn_close",
        "UpgradeScreen_files/Close.png",
        "UpgradeScreen_files/Close.json"
    );
    scene.load.aseprite(
        "btn_upgrades",
        "UpgradeScreen_files/ViewUpgrades.png",
        "UpgradeScreen_files/ViewUpgrades.json"
    );
    scene.load.aseprite(
        "btn_ability",
        "UpgradeScreen_files/UpgradeButton.png",
        "UpgradeScreen_files/UpgradeButton.json"
    );

    // Ability Sprites
    scene.load.aseprite(
        "ab_bruteForce",
        "upgrades/bruteForce.png",
        "upgrades/bruteForce.json"
    );
    scene.load.aseprite(
        "ab_healingMist",
        "upgrades/healingMist.png",
        "upgrades/healingMist.json"
    );
    scene.load.aseprite(
        "healPlayer",
        "upgrades/healPlayer.png",
        "upgrades/healPlayer.json"
    );
    scene.load.aseprite(
        "ab_laser",
        "upgrades/laser.png",
        "upgrades/laser.json"
    );
    scene.load.aseprite(
        "ab_psuOverload",
        "upgrades/psuOverload.png",
        "upgrades/psuOverload.json"
    );
    scene.load.aseprite(
        "ab_railGun",
        "upgrades/railGun.png",
        "upgrades/railGun.json"
    );
    scene.load.aseprite(
        "ab_shotgun",
        "upgrades/shotgun.png",
        "upgrades/shotgun.json"
    );
    scene.load.aseprite(
        "ab_slowField",
        "upgrades/slowField.png",
        "upgrades/slowField.json"
    );
    scene.load.aseprite(
        "slowEnemyIcon",
        "upgrades/slowEnemyIcon.png",
        "upgrades/slowEnemyIcon.json"
    );
    scene.load.aseprite(
        "ab_staticField",
        "upgrades/staticField.png",
        "upgrades/staticField.json"
    );
    scene.load.aseprite(
        "shockEnemyIcon",
        "upgrades/shockEnemyIcon.png",
        "upgrades/shockEnemyIcon.json"
    );
    scene.load.aseprite(
        "ab_teslaCoil",
        "upgrades/teslaCoil.png",
        "upgrades/teslaCoil.json"
    );

    // Audio
    scene.load.audio("atk_healingMist", ["sfx/snd_bot_healingMist.mp3"]);
    scene.load.audio("atk_laser", ["sfx/snd_bot_laserAttachment.mp3"]);
    scene.load.audio("atk_railgun", ["sfx/snd_bot_railGunAttachment.mp3"]);
    scene.load.audio("atk_shotgun1", ["sfx/snd_bot_shotgun01.mp3"]);
    scene.load.audio("atk_shotgun2", ["sfx/snd_bot_shotgun02.mp3"]);
    scene.load.audio("atk_staticField", ["sfx/snd_bot_staticField.mp3"]);
    scene.load.audio("atk_taser", ["sfx/snd_bot_taser.mp3"]);
    scene.load.audio("btn_click", ["sfx/snd_button_click.mp3"]);
    scene.load.audio("btn_hover", ["sfx/snd_button_hover.mp3"]);
    scene.load.audio("collectScrap", ["sfx/snd_collect_scrap.mp3"]);
    scene.load.audio("collectBoss", ["sfx/snd_collectable_boss.mp3"]);
    scene.load.audio("enemy_clink", ["sfx/snd_enemy_clink.mp3"]);
    scene.load.audio("music", ["sfx/snd_gamemusicLoop.mp3"]);
    scene.load.audio("player_death", ["sfx/snd_player_death.mp3"]);
    scene.load.audio("player_hit1", ["sfx/snd_player_hit01.mp3"]);
    scene.load.audio("player_hit2", ["sfx/snd_player_hit02.mp3"]);
    scene.load.audio("player_hit3", ["sfx/snd_player_hit03.mp3"]);
    scene.load.audio("purchase_bruteForce", [
        "sfx/snd_bot_purchase_bruteForce.mp3",
    ]);
    scene.load.audio("purchase_healingMist", [
        "sfx/snd_bot_purchase_healingMist.mp3",
    ]);
    scene.load.audio("purchase_hydroCooledCPU", [
        "sfx/snd_bot_purchase_HydroCooledCPU.mp3",
    ]);
    scene.load.audio("purchase_improvedCameras", [
        "sfx/snd_bot_purchase_ImprovedCameras.mp3",
    ]);
    scene.load.audio("purchase_increasedThroughput", [
        "sfx/snd_bot_purchase_IncreasedThroughput.mp3",
    ]);
    scene.load.audio("purchase_laserAttachment", [
        "sfx/snd_bot_purchase_laserAttachment.mp3",
    ]);
    scene.load.audio("purchase_longerBarrels", [
        "sfx/snd_bot_purchase_longerBarrels.mp3",
    ]);
    scene.load.audio("purchase_myBestFriend", [
        "sfx/snd_bot_purchase_MyBestFriend.mp3",
    ]);
    scene.load.audio("purchase_protectMyFriend", [
        "sfx/snd_bot_purchase_ProtectMyFriend.mp3",
    ]);
    scene.load.audio("purchase_PSUOverclock", [
        "sfx/snd_bot_purchase_PSUOverclock.mp3",
    ]);
    scene.load.audio("purchase_radioJammer", [
        "sfx/snd_bot_purchase_radioJammer.mp3",
    ]);
    scene.load.audio("purchase_railGunAttachment", [
        "sfx/snd_bot_purchase_railGunAttachment.mp3",
    ]);
    scene.load.audio("purchase_shotgunAttachment", [
        "sfx/snd_bot_purchase_shotgunAttachment.mp3",
    ]);
    scene.load.audio("purchase_staticField", [
        "sfx/snd_bot_purchase_staticField.mp3",
    ]);
    scene.load.audio("purchase_tuneUp", ["sfx/snd_bot_purchase_tuneUp.mp3"]);
    scene.load.audio("robot_bossSpawn01", ["sfx/snd_boss_spawn01.mp3"]);
    scene.load.audio("robot_bossSpawn02", ["sfx/snd_boss_spawn02.mp3"]);
    scene.load.audio("robot_bossSpawn03", ["sfx/snd_boss_spawn03.mp3"]);
    scene.load.audio("robot_bossCharge", ["sfx/snd_robot_boss_charge.mp3"]);
    scene.load.audio("robot_meleeDeath1", ["sfx/snd_robot_melee_death01.mp3"]);
    scene.load.audio("robot_meleeDeath2", ["sfx/snd_robot_melee_death02.mp3"]);
    scene.load.audio("robot_rangedDeath1", [
        "sfx/snd_robot_ranged_death01.mp3",
    ]);
    scene.load.audio("robot_rangedDeath2", [
        "sfx/snd_robot_ranged_death02.mp3",
    ]);
};

export const create = (scene) => {
    scene.anims.createFromAseprite("smallAbilities");
    scene.anims.createFromAseprite("player");
    scene.anims.createFromAseprite("ally");
    scene.anims.createFromAseprite("spareParts");
    scene.anims.createFromAseprite("btn_close");
    scene.anims.createFromAseprite("btn_upgrades");
    scene.anims.createFromAseprite("btn_ability");
    scene.anims.createFromAseprite("laserBeam");

    //Abilities
    scene.anims.createFromAseprite("ab_bruteForce");
    scene.anims.createFromAseprite("ab_healingMist");
    scene.anims.createFromAseprite("ab_laser");
    scene.anims.createFromAseprite("ab_psuOverload");
    scene.anims.createFromAseprite("ab_railGun");
    scene.anims.createFromAseprite("ab_shotgun");
    scene.anims.createFromAseprite("ab_slowField");
    scene.anims.createFromAseprite("ab_staticField");
    scene.anims.createFromAseprite("ab_teslaCoil");
    scene.anims.createFromAseprite("slowEnemyIcon");
    scene.anims.createFromAseprite("healPlayer");
    scene.anims.createFromAseprite("shockEnemyIcon");

    //Sounds
    const atk_healingMist = scene.sound.add("atk_healingMist");
    const atk_laser = scene.sound.add("atk_laser");
    const atk_railgun = scene.sound.add("atk_railgun");
    const atk_shotgun1 = scene.sound.add("atk_shotgun1");
    const atk_shotgun2 = scene.sound.add("atk_shotgun2");
    const atk_staticField = scene.sound.add("atk_staticField");
    const atk_taser = scene.sound.add("atk_taser");
    const btn_click = scene.sound.add("btn_click");
    const btn_hover = scene.sound.add("btn_hover");
    const collectScrap = scene.sound.add("collectScrap");
    const collectBoss = scene.sound.add("collectBoss");
    const enemy_clink = scene.sound.add("enemy_clink");
    if (scene.music) {
        scene.music.stop();
    }
    scene.music = scene.sound.add("music", { loop: true });
    const player_death = scene.sound.add("player_death");
    const player_hit1 = scene.sound.add("player_hit1");
    const player_hit2 = scene.sound.add("player_hit2");
    const player_hit3 = scene.sound.add("player_hit3");
    const purchase_bruteForce = scene.sound.add("purchase_bruteForce");
    const purchase_healingMist = scene.sound.add("purchase_healingMist");
    const purchase_hydroCooledCPU = scene.sound.add("purchase_hydroCooledCPU");
    const purchase_improvedCameras = scene.sound.add(
        "purchase_improvedCameras"
    );
    const purchase_increasedThroughput = scene.sound.add(
        "purchase_increasedThroughput"
    );
    const purchase_laserAttachment = scene.sound.add(
        "purchase_laserAttachment"
    );
    const purchase_longerBarrels = scene.sound.add("purchase_longerBarrels");
    const purchase_myBestFriend = scene.sound.add("purchase_myBestFriend");
    const purchase_protectMyFriend = scene.sound.add(
        "purchase_protectMyFriend"
    );
    const purchase_PSUOverclock = scene.sound.add("purchase_PSUOverclock");
    const purchase_radioJammer = scene.sound.add("purchase_radioJammer");
    const purchase_railGunAttachment = scene.sound.add(
        "purchase_railGunAttachment"
    );
    const purchase_shotgunAttachment = scene.sound.add(
        "purchase_shotgunAttachment"
    );
    const purchase_staticField = scene.sound.add("purchase_staticField");
    const purchase_tuneUp = scene.sound.add("purchase_tuneUp");
    const robot_bossSpawn1 = scene.sound.add("robot_bossSpawn01");
    const robot_bossSpawn2 = scene.sound.add("robot_bossSpawn02");
    const robot_bossSpawn3 = scene.sound.add("robot_bossSpawn03");
    const robot_bossCharge = scene.sound.add("robot_bossCharge");
    const robot_meleeDeath1 = scene.sound.add("robot_meleeDeath1");
    const robot_meleeDeath2 = scene.sound.add("robot_meleeDeath2");
    const robot_rangedDeath1 = scene.sound.add("robot_rangedDeath1");
    const robot_rangedDeath2 = scene.sound.add("robot_rangedDeath2");

    const vol = scene.sfxVolume;

    playMusic(vol, scene.music);
    scene.sfx = {
        atk_healingMist: () => playSound(atk_healingMist, vol * 0.5),
        atk_laser: () => playSound(atk_laser, vol * 0.5),
        atk_railgun: () => playSound(atk_railgun, vol),
        atk_shotgun1: () => playSound(atk_shotgun1, vol),
        atk_shotgun2: () => playSound(atk_shotgun2, vol),
        atk_staticField: () => playSound(atk_staticField, vol * 0.2),
        atk_taser: () => playSound(atk_taser, vol * 0.2),
        btn_click: () => playSound(btn_click, vol * 0.8),
        btn_hover: () => playSound(btn_hover, vol * 1.5),
        collectScrap: () => playSound(collectScrap, vol * 0.5),
        collectBoss: () => playSound(collectBoss, vol * 0.2),
        enemy_clink: () => playSound(enemy_clink, vol * 0.5),
        player_death: () => playSound(player_death, vol * 0.2),
        player_hit1: () => playSound(player_hit1, vol * 0.5),
        player_hit2: () => playSound(player_hit2, vol * 0.5),
        player_hit3: () => playSound(player_hit3, vol * 0.5),
        purchase_bruteForce: () => playSound(purchase_bruteForce, vol * 0.5),
        purchase_healingMist: () => playSound(purchase_healingMist, vol * 0.5),
        purchase_hydroCooledCPU: () =>
            playSound(purchase_hydroCooledCPU, vol * 1.2),
        purchase_improvedCameras: () =>
            playSound(purchase_improvedCameras, vol * 1),
        purchase_increasedThroughput: () =>
            playSound(purchase_increasedThroughput, vol * 0.5),
        purchase_laserAttachment: () =>
            playSound(purchase_laserAttachment, vol * 0.5),
        purchase_longerBarrels: () =>
            playSound(purchase_longerBarrels, vol * 0.5),
        purchase_myBestFriend: () =>
            playSound(purchase_myBestFriend, vol * 0.5),
        purchase_protectMyFriend: () =>
            playSound(purchase_protectMyFriend, vol * 0.5),
        purchase_PSUOverclock: () =>
            playSound(purchase_PSUOverclock, vol * 0.5),
        purchase_radioJammer: () => playSound(purchase_radioJammer, vol * 0.5),
        purchase_railGunAttachment: () =>
            playSound(purchase_railGunAttachment, vol * 0.5),
        purchase_shotgunAttachment: () =>
            playSound(purchase_shotgunAttachment, vol * 0.5),
        purchase_staticField: () => playSound(purchase_staticField, vol * 0.5),
        purchase_tuneUp: () => playSound(purchase_tuneUp, vol * 0.5),
        robot_bossSpawn1: () => playSound(robot_bossSpawn1, vol * 1.2),
        robot_bossSpawn2: () => playSound(robot_bossSpawn2, vol * 1.2),
        robot_bossSpawn3: () => playSound(robot_bossSpawn3, vol * 1.2),
        robot_bossCharge: () => playSound(robot_bossCharge, vol * 0.7),
        robot_meleeDeath1: () => playSound(robot_meleeDeath1, vol * 0.2),
        robot_meleeDeath2: () => playSound(robot_meleeDeath2, vol * 0.2),
        robot_rangedDeath1: () => playSound(robot_rangedDeath1, vol * 0.2),
        robot_rangedDeath2: () => playSound(robot_rangedDeath2, vol * 0.2),
    };
};

const playSound = (sound: Phaser.Sound.HTML5AudioSound, volume) => {
    sound.on("complete", () => {
        sound.removeAllListeners();
    });
    sound.play();
    sound.setVolume(volume);
};

const playMusic = (volume, music) => {
    music.play();
    music.setVolume(volume * 0.2);
};
