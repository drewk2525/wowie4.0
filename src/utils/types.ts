type coordinates = {
    x: integer;
    y: integer;
};

type statBarConfig = {
    x?: number;
    y?: number;
    width?: number;
    height?: number;
    color?: number;
    backgroundColor?: number;
    fullAlpha?: number;
    alpha?: number;
    borderSize?: number;
    value: number;
    maxValue: number;
};

type unitConfig = {
    x: number;
    y: number;
    moveSpeed: number;
    attackDamage?: number;
    maxHitpoints?: number;
    spriteScale?: number;
    bodyOffset?: number;
    hitStunTime?: number;
    target?: object;
};

type allyUpgrade = {
    name: string;
    cost: integer;
    units: string;
    craftTime: number;
    description: string;
    callback: Function;
    infiniteBuys?: boolean;
};

type allyTier1 = {
    first: allyUpgrade;
    second: allyUpgrade;
    third: allyUpgrade;
};

type allyTier2 = {
    first: allyUpgrade;
    second: allyUpgrade;
    third: allyUpgrade;
};

type allyTier3 = {
    first: allyUpgrade;
    second: allyUpgrade;
    third: allyUpgrade;
};

type allyPassives = {
    tuneUp: allyUpgrade;
    hydroCooled: allyUpgrade;
    improvedCameras: allyUpgrade;
    psuThroughput?: allyUpgrade;
    protectFriend?: allyUpgrade;
    bestFriend?: allyUpgrade;
};

type allyUpgrades = {
    tier1: allyTier1;
    tier2: allyTier2;
    tier3: allyTier3;
    passives: allyPassives;
};

type ability = {
    name: string;
    myself?: object;
    isEnemy?: boolean;
    range?: number;
    spread?: boolean;
    attackSpeed?: number;
    spriteScale?: number;
    projectiles?: number;
    area?: boolean;
    callback?: Function;
    unlocked?: boolean;
    projectileName?: string;
    projectileSprite?: string;
    projectileSpeed?: number;
    projectileDamage?: number;
    projectileMaxTargets?: number;
    sfx?: string;
    upgradeSprite?: string;
    upgradePosition?: coordinates;
    projectileDelay?: number;
    upgradeSpriteScale?: number;
};

type resources = {
    scrap: number;
    battery: number;
    ram: number;
    cpu: number;
};

type sceneData = {
    sfx: sfxObj;
    resources: resources;
};

type sfxObj = {
    atk_healingMist: Function;
    atk_laser: Function;
    atk_railgun: Function;
    atk_shotgun1: Function;
    atk_shotgun2: Function;
    atk_staticField: Function;
    atk_taser: Function;
    btn_click: Function;
    btn_hover: Function;
    collectScrap: Function;
    collectBoss: Function;
    enemy_clink: Function;
    player_death: Function;
    player_hit1: Function;
    player_hit2: Function;
    player_hit3: Function;
    purchase_bruteForce: Function;
    purchase_healingMist: Function;
    purchase_hydroCooledCPU: Function;
    purchase_improvedCameras: Function;
    purchase_increasedThroughput: Function;
    purchase_laserAttachment: Function;
    purchase_longerBarrels: Function;
    purchase_myBestFriend: Function;
    purchase_protectMyFriend: Function;
    purchase_PSUOverclock: Function;
    purchase_radioJammer: Function;
    purchase_railGunAttachment: Function;
    purchase_shotgunAttachment: Function;
    purchase_staticField: Function;
    purchase_tuneUp: Function;
    robot_bossSpawn1: Function;
    robot_bossSpawn2: Function;
    robot_bossSpawn3: Function;
    robot_bossCharge: Function;
    robot_meleeDeath1: Function;
    robot_meleeDeath2: Function;
    robot_rangedDeath1: Function;
    robot_rangedDeath2: Function;
};
