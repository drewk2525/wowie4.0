import Ally from "../classes/ally";
import BattleScene from "../scenes/BattleScene";

export const collectables = ["sp_ductTape", "sp_nut", "sp_scrap", "sp_bolt"];
export const bossCollectables = ["sp_battery", "sp_ram", "sp_cpu"];

export const getPlayerStats = (scene): unitConfig => ({
    x: scene.scale.width / 2,
    y: scene.scale.height / 2,
    moveSpeed: 250,
    maxHitpoints: 50,
    hitStunTime: 500,
});

export const getAllyStats = (scene): unitConfig => ({
    x: scene.scale.width / 2 + 100,
    y: scene.scale.height / 2,
    moveSpeed: 65,
    attackDamage: 3,
});

const payUpgradeCost = (scene: BattleScene, cost: number) => {
    if (scene.scrap >= cost) {
        scene.addResource("scrap", -cost);
        return true;
    } else {
        return false;
    }
};

export const runUpgradeCallback = (scene, upgrade: allyUpgrade) => {
    if (payUpgradeCost(scene, upgrade.cost)) {
        upgrade.callback(scene);
    }
};

export const getAllyUpgrades = (): allyUpgrades => {
    return {
        tier1: {
            first: {
                name: "PSU Overclock",
                cost: 1,
                craftTime: 500,
                units: "battery",
                description: "Increase Roby's taser size & damage.",
                callback: (scene: BattleScene) => {
                    const taser = scene.ally.getAbilityByName("taser");
                    taser.projectileDamage *= 2;
                    taser.spriteScale *= 2;
                    scene.sfx.purchase_PSUOverclock();
                },
            },
            second: {
                name: "Radio Jammer",
                cost: 1,
                craftTime: 500,
                units: "battery",
                description: "Slow enemies in a range.",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_staticField();
                },
            },
            third: {
                name: "Tesla Coil",
                cost: 1,
                craftTime: 500,
                units: "battery",
                description: "Roby's taser is now a long-range projectile.",
                callback: (scene: BattleScene) => {
                    const taser = scene.ally.getAbilityByName("taser");
                    taser.range *= 4;
                    scene.sfx.purchase_longerBarrels();
                },
            },
        },
        tier2: {
            first: {
                name: "Healing Mist",
                cost: 1,
                craftTime: 800,
                units: "ram",
                description: "Roby emits a healing mist every few seconds.",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_healingMist();
                },
            },
            second: {
                name: "Laser Gun Arm",
                cost: 1,
                craftTime: 800,
                units: "ram",
                description:
                    "Fires a laser in a straight line, passing through enemies.",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_laserAttachment();
                },
            },
            third: {
                name: "Brute Force",
                cost: 1,
                craftTime: 800,
                units: "ram",
                description:
                    "Enemies take higher damage the closer they are to Roby.",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_bruteForce();
                    scene.ally.bruteForce = true;
                },
            },
        },
        tier3: {
            first: {
                name: "Rail Gun Arm",
                cost: 1,
                craftTime: 1300,
                units: "cpu",
                description: "Fires a burst of 3 projectiles.",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_railGunAttachment();
                },
            },
            second: {
                name: "Shotgun Arm",
                cost: 1,
                craftTime: 1300,
                units: "cpu",
                description:
                    "Shoots multiple projectiles in a short ranged cone.",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_shotgunAttachment();
                },
            },
            third: {
                name: "Static Field",
                cost: 1,
                craftTime: 1300,
                units: "cpu",
                description: "Large AOE that damages all enemies in range.",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_staticField();
                },
            },
        },
        passives: {
            tuneUp: {
                name: "Tune Up",
                cost: 1,
                craftTime: 500,
                infiniteBuys: true,
                units: "scrap",
                description: "Inrease Roby's movement speed by 5%",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_tuneUp();
                    scene.ally.moveSpeed = scene.ally.moveSpeed * 1.05;
                },
            },
            hydroCooled: {
                name: "Hydro-cooled CPU",
                cost: 1,
                craftTime: 500,
                infiniteBuys: true,
                units: "scrap",
                description: "Increase Roby's ability speed by 5%",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_hydroCooledCPU();
                    scene.ally.baseAbilitySpeed =
                        scene.ally.baseAbilitySpeed -
                        scene.ally.baseAbilitySpeed * 0.02;
                },
            },
            improvedCameras: {
                name: "Improved Cameras",
                cost: 5,
                craftTime: 500,
                infiniteBuys: true,
                units: "scrap",
                description: "Inreases Roby's sight range by 5%",
                callback: (scene: BattleScene) => {
                    scene.sfx.purchase_improvedCameras();
                    scene.ally.aggroCircle.radius =
                        scene.ally.aggroCircle.radius * 1.05;
                },
            },
            // psuThroughput: {
            //     name: "PSU Throughput",
            //     cost: 10,
            //     craftTime: 500,
            //     infiniteBuys: true,
            //     units: "scrap",
            //     description: "Increase Ability Range by 5%",
            //     callback: (scene: BattleScene) => {
            //         scene.sfx.purchase_increasedThroughput();
            //         scene.ally.baseRange = scene.ally.baseRange * 1.05;
            //     },
            // },
            // protectFriend: {
            //     name: "Protect My Friend",
            //     cost: 25,
            //     craftTime: 500,
            //     infiniteBuys: true,
            //     units: "scrap",
            //     description: "Roby increases your max health by 5",
            //     callback: (scene: BattleScene) => {
            //         scene.sfx.purchase_protectMyFriend();
            //         scene.player.maxHitpoints += 5;
            //         scene.player.hitpoints += 5;
            //         scene.player.hitpointBar.update(5, 5);
            //     },
            // },
            // bestFriend: {
            //     name: "Last?",
            //     cost: 20,
            //     craftTime: 500,
            //     units: "scrap",
            //     description: "third upgrade",
            //     callback: (scene: BattleScene) => {
            //         scene.sfx.purchase_myBestFriend();
            //     },
            // },
        },
    };
};
