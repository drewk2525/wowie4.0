export const createResourceCounters = (scene) => {
    resourceCounter(scene, "scrap");
    resourceCounter(scene, "battery", 140);
    resourceCounter(scene, "ram", 280);
    resourceCounter(scene, "cpu", 420);
};

export const resourceCounter = (
    scene: Phaser.Scene,
    type: string,
    xOffset = 0
) => {
    scene[type] = scene[type] || 0;
    const offset = 60;
    scene[`${type}Icon`] = scene.add.image(
        offset + xOffset,
        offset,
        `${type === "scrap" ? "icon_spareParts" : type}`
    );
    scene[`${type}Icon`].setScale(0.5);
    scene[`${type}Text`] = scene.add.text(
        (xOffset === 0 ? 60 : 40) + offset + xOffset,
        offset - 5,
        scene[type],
        {
            // @ts-ignore
            fontSize: 40,
            fontFamily: "sans-serif",
            color: "#bb9966",
        }
    );
    scene[`${type}Text`].setOrigin(0.5);
};
