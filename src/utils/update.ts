import BattleScene from "../scenes/BattleScene";

export const updateFunction = (scene: BattleScene) => {
    scene.player.update();
    scene.ally.update();
    scene.enemySpawner.group.children.entries.forEach((enemy) => {
        const self = enemy.data.values.self;
        self.update();
    });
    scene.collectableGroup.children.entries.forEach((collectable) => {
        const self = collectable.data.values.self;
        self.update();
    });
    const { x, y } = scene.input.activePointer;
    scene.tooltip.x = x + scene.tooltipBackground.width / 2;
    scene.tooltip.y = y + scene.tooltipBackground.height / 2 + 20;
    if (scene.tooltip.x > 1720) {
        scene.tooltip.x = scene.tooltip.x - 200;
    }
    if (scene.tooltip.y > 880) {
        scene.tooltip.y = scene.tooltip.y - 120;
    }

    // Check projectile distance
    scene.allyProjectileGroup.getChildren().forEach((proj) => {
        proj.data.values.distanceTraveled +=
            // @ts-ignore
            proj.body.deltaAbsX() + proj.body.deltaAbsY();
        if (proj.data.values.distanceTraveled > proj.data.values.maxDistance) {
            proj.destroy();
        }
    });
};
