import { getAllyUpgrades, runUpgradeCallback } from "./data";

export const createKeyBindings = (scene) => {
    const kc = Phaser.Input.Keyboard.KeyCodes;
    scene.keys = scene.input.keyboard.addKeys({
        up: kc.W,
        left: kc.A,
        down: kc.S,
        right: kc.D,
        up2: kc.UP,
        left2: kc.LEFT,
        down2: kc.DOWN,
        right2: kc.RIGHT,
        ability1: kc.J,
        ability2: kc.K,
        ability3: kc.L,
        ability4: kc.SPACE,
        pause: kc.ESC,
        craft: kc.F,
        craft2: kc.TAB,
        passive1: kc.ONE,
        passive2: kc.TWO,
        passive3: kc.THREE,
        passive4: kc.FOUR,
        passive5: kc.FIVE,
        passive6: kc.SIX,
        debug: kc.ZERO,
    });
    const moveBtnUp = (dir) => {
        scene.player.idle();
        // Idle Character
    };
    scene.keys.up.on("up", () => {
        moveBtnUp("Up");
    });
    scene.keys.up2.on("up", () => {
        moveBtnUp("Up");
    });
    scene.keys.left.on("up", () => {
        moveBtnUp("Left");
    });
    scene.keys.left2.on("up", () => {
        moveBtnUp("Left");
    });
    scene.keys.down.on("up", () => {
        moveBtnUp("Down");
    });
    scene.keys.down2.on("up", () => {
        moveBtnUp("Down");
    });
    scene.keys.right.on("up", () => {
        moveBtnUp("Right");
    });
    scene.keys.right2.on("up", () => {
        moveBtnUp("Right");
    });

    // Craft / Abilities
    scene.keys.craft.on("down", () => {
        scene.launchCraftMenu();
    });
    scene.keys.craft2.on("down", () => {
        scene.launchCraftMenu();
    });

    // const passives = getAllyUpgrades().passives;
    // const keys = Object.keys(passives);
    // scene.keys.passive1.on("down", () => {
    //     runUpgradeCallback(scene, passives[keys[0]]);
    // });
    // scene.keys.passive2.on("down", () => {
    //     runUpgradeCallback(scene, passives[keys[1]]);
    // });
    // scene.keys.passive3.on("down", () => {
    //     runUpgradeCallback(scene, passives[keys[2]]);
    // });
    // scene.keys.passive4.on("down", () => {
    //     runUpgradeCallback(scene, passives[keys[3]]);
    // });
    // scene.keys.passive5.on("down", () => {
    //     runUpgradeCallback(scene, passives[keys[4]]);
    // });
    // scene.keys.passive6.on("down", () => {
    //     runUpgradeCallback(scene, passives[keys[5]]);
    // });

    scene.keys.debug.on("down", () => {
        scene.physics.config.debug = !scene.physics.config.debug;
    });

    // scene.keys.up.on("down", () => {
    //     // scene.player.move("Up");
    //     scene.player.direction = "Up";
    // });
    // scene.keys.up2.on("down", () => {
    //     scene.player.move("Up");
    // });
    // scene.keys.left.on("down", () => {
    //     scene.player.move("Left");
    // });
    // scene.keys.left2.on("down", () => {
    //     scene.player.move("Left");
    // });
    // scene.keys.down.on("down", () => {
    //     scene.player.move("Down");
    // });
    // scene.keys.down2.on("down", () => {
    //     scene.player.move("Down");
    // });
    // scene.keys.right.on("down", () => {
    //     scene.player.move("Right");
    // });
    // scene.keys.right2.on("down", () => {
    //     scene.player.move("Right");
    // });

    // Ability Buttons
    // scene.keys.ability1.on("down", () => {
    //     characterAttack(scene, scene.hero, "ability1");
    // });
    // scene.keys.ability2.on("down", () => {
    //     characterAttack(scene, scene.hero, "ability2");
    // });

    // Other
    scene.keys.pause.on("down", () => {
        scene.scene.launch("paused");
        scene.scene.pause();
    });
};
