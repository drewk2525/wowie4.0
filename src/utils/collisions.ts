import Ally from "../classes/ally";
import Collectable from "../classes/Collectable";
import Enemy from "../classes/Enemy";
import BattleScene from "../scenes/BattleScene";

export const onWorldBounds = (body) => {
    // if (body.gameObject.ability) {
    //     destroyProjectile(body);
    // }
};

export const setCollisions = (scene: BattleScene) => {
    addPlayerCollisions(scene);
    addAllyCollisions(scene);
    allyProjectileCollisions(scene);
    enemyProjectileCollisions(scene);
};

const addPlayerCollisions = (scene: BattleScene) => {
    // Collectable Collision
    scene.physics.add.collider(
        scene.player.container,
        scene.collectableGroup,
        (player, collectable) => {
            if (player.data.values.self.isAlive()) {
                const self: Collectable = collectable.data.values.self;
                self.collect();
            }
        }
    ).overlapOnly = true;

    // Enemy Collision
    scene.physics.add.collider(
        scene.player.container,
        scene.enemySpawner.group,
        (player, enemy) => {
            if (
                player.data.values.self.isAlive() &&
                !player.data.values.self.invincible
            ) {
                player.data.values.self.damageBounceBack(enemy);
            }
            player.data.values.self.takeDamage(
                enemy.data.values.self.attackDamage
            );
        }
    ).overlapOnly = true;
};

const addAllyCollisions = (scene: BattleScene) => {
    scene.physics.add.collider(
        scene.ally.container,
        scene.enemySpawner.group,
        (ally, enemy) => {
            if (enemy.data && !enemy.data.values.self.isBoss) {
                enemy.data.values.self.damageBounceBack(ally);
            }
        }
    ).overlapOnly = true;
};

const allyProjectileCollisions = (scene: BattleScene) => {
    scene.physics.add.collider(
        scene.allyProjectileGroup,
        scene.enemySpawner.group,
        (projectile, e) => {
            if (
                !projectile.data ||
                projectile.data.values.targetsRemaining <= 0
            ) {
                return;
            }
            projectile.data.values.targetsRemaining -= 1;
            const enemy: Enemy = e.data.values.self;
            enemy.takeDamage(projectile.data.values.damage);
            if (projectile.data.values.targetsRemaining <= 0) {
                projectile.destroy();
            }
        }
    ).overlapOnly = true;
};

const enemyProjectileCollisions = (scene: BattleScene) => {
    scene.physics.add.collider(
        scene.enemyProjectileGroup,
        scene.player.container,
        (p, projectile) => {
            if (
                !projectile.data ||
                projectile.data.values.targetsRemaining <= 0
            ) {
                return;
            }
            const player = p.data.values.self;
            if (player.isAlive() && !player.invincible) {
                player.damageBounceBack(projectile);
            }
            projectile.data.values.targetsRemaining -= 1;
            player.takeDamage(projectile.data.values.damage);
            if (projectile.data.values.targetsRemaining <= 0) {
                projectile.destroy();
            }
        }
    );
};
