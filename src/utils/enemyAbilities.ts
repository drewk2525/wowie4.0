export const enemyAbilities: Array<ability> = [
    {
        name: "clink",
        range: 800,
        isEnemy: true,
        attackSpeed: 4000,
        spriteScale: 2,
        projectiles: 1,
        projectileSpeed: 300,
        projectileName: "redClink",
        projectileDamage: 5,
        projectileMaxTargets: 1,
        sfx: "enemy_clink",
        unlocked: true,
    },
];
