// @ts-nocheck
export default class GameOverScene extends Phaser.Scene {
    constructor() {
        super("gameOver");
    }

    create({ win }) {
        const rectangle = this.add.rectangle(
            0,
            0,
            this.scale.width,
            this.scale.height,
            0x555555,
            0.7
        );
        rectangle.setAlpha(0);
        rectangle.setOrigin(0);
        const textProps = {
            fontFamily: "sans-serif",
            align: "center",
            fontSize: 120,
            color: "#ffffff",
        };
        const gameOverText = win ? "You win!" : "GAME OVER";
        const text = this.add.text(
            this.scale.width / 2,
            this.scale.height / 2,
            gameOverText,
            textProps
        );
        text.setAlpha(0);
        text.setOrigin(0.5);

        const duration = 80;

        win && this.createContinuePlayingButton(rectangle, textProps);

        const retryText = this.add.text(
            this.scale.width / 2,
            this.scale.height / 2 + 200,
            "Restart",
            { ...textProps, fontSize: 60 }
        );
        retryText.setAlpha(0);
        retryText.setOrigin(0.5);
        retryText.setInteractive();
        retryText.on("pointerover", () => {
            this.add.tween({
                targets: retryText,
                scaleX: 1.2,
                scaleY: 1.2,
                duration,
            });
        });
        retryText.on("pointerout", () => {
            this.add.tween({
                targets: retryText,
                scaleX: 1,
                scaleY: 1,
                duration,
            });
        });
        retryText.on("pointerdown", () => {
            this.scene.start("battle");
            this.scene.stop();
        });
        this.add.tween({
            targets: rectangle,
            alpha: 1,
            duration: 2500,
            repeat: 0,
            onComplete: () => {
                retryText.setAlpha(1);
                text.setAlpha(1);
            },
        });
    }

    update() {}

    createContinuePlayingButton(rectangle, textProps) {
        const duration = 80;
        const continueText = this.add.text(
            this.scale.width / 2,
            this.scale.height / 2 + 100,
            "Continue",
            { ...textProps, fontSize: 60 }
        );
        continueText.setAlpha(0);
        continueText.setOrigin(0.5);
        continueText.setInteractive();
        continueText.on("pointerover", () => {
            this.add.tween({
                targets: continueText,
                scaleX: 1.2,
                scaleY: 1.2,
                duration,
            });
        });
        continueText.on("pointerout", () => {
            this.add.tween({
                targets: continueText,
                scaleX: 1,
                scaleY: 1,
                duration,
            });
        });
        continueText.on("pointerdown", () => {
            this.scene.resume("battle");
            this.scene.stop();
        });
        this.add.tween({
            targets: rectangle,
            alpha: 1,
            duration: 2500,
            repeat: 0,
            onComplete: () => {
                continueText.setAlpha(1);
            },
        });
    }
}
