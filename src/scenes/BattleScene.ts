// @ts-nocheck
import Phaser, { Tweens } from "phaser";
import Ally from "../classes/ally";
import EnemySpawner from "../classes/EnemySpawner";
import Player from "../classes/Player";
import { setCollisions } from "../utils/collisions";
import { getAllyStats, getPlayerStats } from "../utils/data";
import { createKeyBindings } from "../utils/keyBindings";
import { create, preload } from "../utils/preload";
import { createResourceCounters } from "../utils/sharedSceneMethods";
import { updateFunction } from "../utils/update";

export default class BattleScene extends Phaser.Scene {
    customBounds: Phaser.Geom.Rectangle;
    player: Player;
    ally: Ally;
    enemySpawner: EnemySpawner;
    collectableGroup: Phaser.GameObjects.Group;
    allyProjectileGroup: Phaser.GameObjects.Group;
    enemyProjectileGroup: Phaser.GameObjects.Group;
    scrap: number;
    battery: number;
    ram: number;
    cpu: number;
    bossesKilled: number;
    bossesKilledMax: number;
    bossesKilledText: Phaser.GameObjects.Text;
    scrapText: string;
    batteryText: string;
    ramText: string;
    cpuText: string;
    scrapIcon: Phaser.GameObjects.GameObject;
    batteryIcon: Phaser.GameObjects.GameObject;
    ramIcon: Phaser.GameObjects.GameObject;
    cpuIcon: Phaser.GameObjects.GameObject;
    tooltip: Phaser.GameObjects.Container;
    tooltipText: Phaser.GameObjects.Text;
    tooltipBackground: Phaser.GameObjects.Rectangle;
    sfxVolume: number;
    sfx: sfxObj;
    music: integer;
    updateTutorialTip: Tweens.Tween;
    moveTutorialTip: Tweens.Tween;

    constructor() {
        super("battle");
    }

    preload() {
        preload(this);
    }

    create() {
        this.sfxVolume = 0.2;
        create(this);

        this.backgroundImage = this.add.image(
            this.scale.width / 2,
            this.scale.height / 2,
            "background"
        );
        this.backgroundImage.name = "background";
        const backgroundFilter = this.add.rectangle(
            0,
            0,
            this.scale.width,
            this.scale.height,
            0x663333,
            0.3
        );
        backgroundFilter.setOrigin(0);
        this.customBounds = new Phaser.Geom.Rectangle(40, 40, 1840, 1000);
        this.player = new Player(this, "player", getPlayerStats(this));
        this.ally = new Ally(this, "ally", getAllyStats(this));
        this.enemySpawner = new EnemySpawner(this);
        this.bossesKilled = 0;
        this.bossesKilledMax = 10;
        this.scrap = 0;
        this.cpu = 0;
        this.ram = 0;
        this.battery = 0;

        this.collectableGroup = this.physics.add.group();
        this.allyProjectileGroup = this.physics.add.group();
        this.enemyProjectileGroup = this.physics.add.group();
        setCollisions(this);
        createKeyBindings(this);
        this.addUpdateTutorialTip();
        this.addMoveTutorialTip();

        createResourceCounters(this);

        const textProps = {
            fontSize: 20,
            fontFamily: "sans-serif",
            color: "#ffffff",
            align: "left",
            fixedWidth: 180,
            fixedHeight: 90,
            wordWrap: {
                width: 180,
            },
        };

        this.tooltip = this.add.container(0, 0);
        this.tooltipBackground = this.add.rectangle(0, 0, 200, 100, 0x333333);
        this.tooltipText = this.add.text(0, 0, "tooltip text", textProps);
        this.tooltipText.setOrigin(0.5);
        this.tooltip.add(this.tooltipBackground);
        this.tooltip.add(this.tooltipText);
        this.tooltip.setAlpha(0);
        this.tooltip.setDepth(200);

        const upgradeButton = this.add.sprite(1860, 60, "btn_upgrades");
        upgradeButton.setInteractive();
        upgradeButton.on("pointerout", () => {
            this.tooltip.setAlpha(0);
            upgradeButton.setFrame(0);
        });
        upgradeButton.on("pointerover", () => {
            this.tooltipText.text = "Upgrade Menu\n(F or TAB)";
            this.sfx.btn_hover();
            this.tooltip.setAlpha(1);
            upgradeButton.setFrame(1);
        });
        upgradeButton.on("pointerdown", () => {
            this.sfx.btn_click();
            upgradeButton.setFrame(2);
            this.time.addEvent({
                delay: 100,
                callback: () => {
                    this.launchCraftMenu();
                },
            });
        });
        upgradeButton.on("pointerup", () => {
            upgradeButton.setFrame(0);
        });

        this.bossesKilledText = this.add.text(
            20,
            100,
            `Bosses killed: ${this.bossesKilled}/${this.bossesKilledMax}`,
            textProps
        );
        this.bossesKilledText.setAlpha(1);
    }

    launchCraftMenu() {
        if (!this.ally.abilities) {
            return;
        }
        if (this.updateTutorialTip) {
            this.updateTutorialTip.targets[0].destroy();
            this.updateTutorialTip.complete();
        }
        this.tooltip.setAlpha(0);
        this.scene.launch("craft", this);
        this.scene.pause();
    }

    addResource(type: string, amount?: number, ignoreTween?: boolean = false) {
        this[type] += amount || 1;
        this[`${type}Text`].text = `${this[type]}`;
        if (ignoreTween) {
            return;
        }
        const resourceText = this.add.text(
            this[`${type}Text`].x,
            this[`${type}Text`].y,
            this[type],
            {
                fontSize: 40,
                fontFamily: "sans-serif",
                color: "#CC9955",
            }
        );
        resourceText.setOrigin(0.5);
        this.add.tween({
            targets: resourceText,
            scaleX: 1.5,
            scaleY: 1.5,
            duration: 120,
            // ease: "Quint.easeIn",
            yoyo: true,
            repeat: 0,
            onComplete: () => {
                resourceText.destroy();
            },
        });
    }

    addUpdateTutorialTip() {
        const container = this.add.container(1650, 40);
        const text = this.add.text(0, 0, "Click here\nfor upgrades!", {
            fontSize: 20,
            fontFamily: "sans-serif",
            color: "#d8d45c",
        });
        const arrow = this.add.image(140, 20, "arrow");
        container.add(text);
        container.add(arrow);
        this.updateTutorialTip = this.add.tween({
            targets: container,
            x: container.x + 30,
            duration: 500,
            // ease: "Quint.easeIn",
            yoyo: true,
            repeat: -1,
        });
    }

    addMoveTutorialTip() {
        const container = this.add.container(850, 40);
        const text = this.add.text(0, 0, "WASD / Arrows to move", {
            fontSize: 20,
            fontFamily: "sans-serif",
            color: "#d8d45c",
        });
        container.add(text);
        this.moveTutorialTip = this.add.tween({
            targets: container,
            y: container.y + 30,
            duration: 500,
            // ease: "Quint.easeIn",
            yoyo: true,
            repeat: -1,
        });
    }

    update() {
        updateFunction(this);
    }
}
