// @ts-nocheck
export default class PausedScene extends Phaser.Scene {
    constructor() {
        super("paused");
    }

    create() {
        const rectangle = this.add.rectangle(
            0,
            0,
            this.scale.width,
            this.scale.height,
            0x555555,
            0.5
        );
        rectangle.setOrigin(0);
        const textProps = {
            fontFamily: "sans-serif",
            align: "center",
            fontSize: 120,
            color: "#ffffff",
        };
        const text = this.add.text(
            this.scale.width / 2,
            this.scale.height / 2,
            "Paused",
            textProps
        );
        text.setOrigin(0.5);
        const esc = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.ESC
        );
        esc.on("down", () => {
            this.scene.resume("battle");
            this.scene.stop();
        });
    }

    update() {}
}
