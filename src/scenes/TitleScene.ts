// @ts-nocheck
import Phaser from "phaser";

export default class TitleScene extends Phaser.Scene {
    constructor() {
        super("title");
    }

    preload() {
        this.load.image("background-title", "splash_MyFriendRoby.png");
        this.load.audio("mainMenu_Music", ["sfx/snd_mainmenuloop.wav"]);
        this.load.audio("btn_click", ["sfx/snd_button_click.mp3"]);
        this.load.audio("btn_hover", ["sfx/snd_button_hover.mp3"]);
    }

    create() {
        const background = this.add.image(960, 540, "background-title");
        background.setOrigin(0.5);
        background.setScale(1.25);
        const music = this.sound.add("mainMenu_Music", { loop: true });
        this.playMusic(0.25, music);

        const btn_click = this.sound.add("btn_click");
        const btn_hover = this.sound.add("btn_hover");

        const textProps = {
            fontFamily: "sans-serif",
            align: "center",
            fontSize: 80,
            color: "#ffffff",
        };
        const titleText = this.add.text(
            this.scale.width / 2,
            60,
            "My Friend Roby",
            textProps
        );
        titleText.setOrigin(0.5);

        const playText = this.add.text(this.scale.width / 2, 675, "Play", {
            ...textProps,
            stroke: "#333333",
            strokeThickness: 3,
        });
        playText.setOrigin(0.5);
        playText.setInteractive();

        const duration = 80;
        playText.on("pointerover", () => {
            this.add.tween({
                targets: playText,
                scaleX: 1.2,
                scaleY: 1.2,
                duration,
            });
            this.playSound(btn_hover, 0.25 * 1.5);
        });
        playText.on("pointerout", () => {
            this.add.tween({
                targets: playText,
                scaleX: 1,
                scaleY: 1,
                duration,
            });
        });
        playText.on("pointerdown", () => {
            this.playSound(btn_click, 0.25 * 0.8);
            music.stop();
            this.scene.start("battle");
            this.scene.stop("title");
        });
    }

    playSound(sound: Phaser.Sound.HTML5AudioSound, volume) {
        sound.on("complete", () => {
            sound.removeAllListeners();
        });
        sound.play();
        sound.setVolume(volume);
    }

    playMusic(volume, music) {
        music.play();
        music.setVolume(volume * 0.2);
    }
}
