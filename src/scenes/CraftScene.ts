import { getAllyUpgrades } from "../utils/data";
import {
    createResourceCounters,
    resourceCounter,
} from "../utils/sharedSceneMethods";
import BattleScene from "./BattleScene";

// @ts-nocheck
export default class CraftScene extends Phaser.Scene {
    sceneData: BattleScene;
    scrap: number;
    battery: number;
    ram: number;
    cpu: number;

    constructor() {
        super("craft");
    }

    create(data: BattleScene) {
        this.sceneData = data;
        this.scrap = data.scrap;
        this.battery = data.battery;
        this.ram = data.ram;
        this.cpu = data.cpu;
        const rectangle = this.add.rectangle(
            0,
            0,
            this.scale.width,
            this.scale.height,
            0x555555,
            0.8
        );
        rectangle.setOrigin(0);
        const backgroundImage = this.add.image(
            this.scale.width / 2,
            this.scale.height / 2,
            "upgradeScreenBackground"
        );
        this.keyBindings();
        this.closeButton();
        for (let i = 0; i < 3; i++) {
            this.createTier(i);
        }
        createResourceCounters(this);
        this.createPassiveButtons();
    }

    createTier(tierNum) {
        const xOffset = 565 * tierNum;
        const tierContainer = this.add.container(130 + xOffset, 200);
        const tierBorder = this.add.image(0, 0, "upgradeTierBorder");
        const titleText = this.add.text(
            tierBorder.width / 2,
            25,
            `Tier ${tierNum + 1} Upgrades`,
            {
                // @ts-ignore
                fontSize: 30,
                fontFamily: "sans-serif",
                color: "#ffffff",
            }
        );
        titleText.setOrigin(0.5);
        tierBorder.setOrigin(0);
        tierContainer.add(tierBorder);
        tierContainer.add(titleText);

        const upgrades = getAllyUpgrades()[`tier${tierNum + 1}`];
        Object.keys(upgrades).forEach((key, i) => {
            const upgrade = upgrades[key];
            const x = 20;
            const y = 50 + i * 130;
            this.createUpgradeButton({
                x,
                y,
                upgrade,
                container: tierContainer,
            });
        });
    }

    createPassiveButtons() {
        const upgrades = getAllyUpgrades().passives;
        Object.keys(upgrades).forEach((key, i) => {
            const upgrade = upgrades[key];
            const x = 150 + (i % 3) * 565;
            const y =
                740 + (i < 3 && Object.keys(upgrades).length > 3 ? 0 : 120);
            const passiveCounts = this.sceneData.ally.passiveCounts;
            const count = passiveCounts[upgrade.name];
            const power = 1.5;
            upgrade.cost = count
                ? Math.floor(Math.pow(count * upgrade.cost, power))
                : upgrade.cost;
            this.createUpgradeButton({ x, y, upgrade, container: null });
        });
        const passiveText = this.add.text(
            this.scale.width / 2,
            700 + (Object.keys(upgrades).length > 3 ? 0 : 120),
            "Passive Abilties",
            {
                // @ts-ignore
                fontSize: 30,
                fontFamily: "sans-serif",
                color: "#ffffff",
            }
        );
        passiveText.setOrigin(0.5);
    }

    createUpgradeButton({ x, y, upgrade, container }) {
        const upgradeContainer = this.add.container(x, y);
        const active =
            this.sceneData[upgrade.units] >= upgrade.cost &&
            !this.sceneData.ally.isAbilityUnlocked(upgrade);
        // upgradeContainer.setData("active", active);
        const background = this.add.sprite(0, 0, "btn_ability");
        if (!active) {
            background.setTint(0x999999);
        }
        upgradeContainer.setInteractive(
            new Phaser.Geom.Rectangle(
                0,
                0,
                background.width,
                background.height
            ),
            Phaser.Geom.Rectangle.Contains
        );
        background.setOrigin(0);

        const textProps = {
            fontSize: 25,
            fontFamily: "sans-serif",
            color: "#333333",
            align: "left",
            fixedWidth: background.width - 20,
            fixedHeight: background.height - 60,
            wordWrap: {
                width: background.width - 20,
            },
        };

        // @ts-ignore
        const nameText = this.add.text(10, 10, upgrade.name, textProps);

        const descriptionText = this.add.text(10, 40, upgrade.description, {
            ...textProps,
            // @ts-ignore
            fontSize: 20,
        });

        const xOffset = upgrade.cost.toString().length * 10 + 35;
        const costText = this.add.text(
            background.width - xOffset,
            10,
            `${upgrade.cost}`,
            {
                ...textProps,
                // @ts-ignore
                fontSize: 20,
            }
        );

        const isScrap = upgrade.units === "scrap";
        const costUnit = this.add.image(
            background.width - 20,
            20,
            isScrap ? "icon_spareParts" : upgrade.units
        );

        costUnit.setScale(isScrap ? 0.2 : 0.4);

        upgradeContainer.on("pointerout", () => {
            background.setFrame(0);
        });
        upgradeContainer.on("pointerover", () => {
            if (!active) {
                return;
            }
            this.sceneData.sfx.btn_hover();
            background.setFrame(1);
        });
        upgradeContainer.on("pointerdown", () => {
            if (!active) {
                return;
            }
            this.purchaseUpgrade(upgrade);
            background.setFrame(2);
        });
        upgradeContainer.on("pointerup", () => {
            if (!active) {
                return;
            }
            background.setFrame(1);
        });

        upgradeContainer.add(background);
        upgradeContainer.add(nameText);
        upgradeContainer.add(descriptionText);
        upgradeContainer.add(costText);
        upgradeContainer.add(costUnit);
        if (container) {
            container.add(upgradeContainer);
        }
    }

    purchaseUpgrade(upgrade: allyUpgrade) {
        if (this.sceneData[upgrade.units] < upgrade.cost) {
            return;
        }
        const ally = this.sceneData.ally;
        ally.addAbility(upgrade);
        this.sceneData.addResource(upgrade.units, -upgrade.cost, true);
        upgrade.callback(this.sceneData);
        this.scene.restart();
    }

    closeButton() {
        const closeButton = this.add.sprite(1800, 107, "btn_close");
        closeButton.setInteractive();
        closeButton.on("pointerout", () => {
            closeButton.setFrame(0);
        });
        closeButton.on("pointerover", () => {
            this.sceneData.sfx.btn_hover();
            closeButton.setFrame(1);
        });
        closeButton.on("pointerdown", () => {
            this.sceneData.sfx.btn_click();
            closeButton.setFrame(2);
            this.resume();
        });
        closeButton.on("pointerup", () => {
            closeButton.setFrame(0);
        });
    }

    resume() {
        this.scene.resume("battle");
        this.scene.stop();
    }

    keyBindings() {
        const esc = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.ESC
        );
        const f = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F);
        const tab = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.TAB
        );
        esc.on("down", () => {
            this.resume();
        });
        f.on("down", () => {
            this.resume();
        });
        tab.on("down", () => {
            this.resume();
        });
    }

    update() {}
}
