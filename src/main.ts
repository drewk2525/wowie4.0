import Phaser from "phaser";

import BattleScene from "./scenes/BattleScene";
import PausedScene from "./scenes/PausedScene";
import GameOverScene from "./scenes/GameOverScene";
import CraftScene from "./scenes/CraftScene";
import TitleScene from "./scenes/TitleScene";

const width = 1920 / window.devicePixelRatio;
const height = 1080 / window.devicePixelRatio;

const config = {
    type: Phaser.AUTO,
    width,
    height,
    physics: {
        default: "arcade",
        arcade: {
            debug: false,
        },
    },
    scene: [TitleScene, BattleScene, PausedScene, GameOverScene, CraftScene],
    scale: {
        parent: "gameBody",
        mode: Phaser.Scale.ScaleModes.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        zoom: 1 / window.devicePixelRatio,
        width: width * window.devicePixelRatio,
        height: height * window.devicePixelRatio,
    },
};

export default new Phaser.Game(config);
